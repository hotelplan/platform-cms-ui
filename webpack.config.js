let CopyWebpackPlugin = require('copy-webpack-plugin');
let CloudCmsPlugin = require('cloudcms-webpack-plugin');
const Path = require('node:path');

module.exports = {
  "context": process.cwd(),
  "entry": ["./index.js"],
  "output": {
    "path": __dirname + "/dist",
    "filename": "index.js",
    "libraryTarget": "amd"
  },
  "externals": [
    CloudCmsPlugin.externalFn
  ],
  "optimization": {
    minimize: true
  },
  "module": {
    "rules": [{
      // write out CSS referenced files into assets directory
      test: /\.(jpe?g|png|gif|woff)$/i,
      loader: "file-loader",
      options: {
        name: '[path][name]-[hash].[ext]',
        outputPath: 'assets/'
      }
    }, {
      // support for inline css within modules
      "test": /\.css$/,
      "use": [{
        "loader": "css-loader",
        "options": {
          "minimize": true
        }
      }]
    }, {
      // support for inline html within modules
      "test": /\.html$/,
      "use": [{
        "loader": "raw-loader"
      }]
    }, {
      // support for inline text within modules
      "test": /\.txt$/,
      "use": [{
        "loader": "raw-loader"
      }]
    }]
  },
  "plugins": [
    new CopyWebpackPlugin(
        {
          patterns: [
            // { from: Path.resolve('./install.js'), to: './dist' },
            {"from": Path.resolve('./install.js')},
            {"from": Path.resolve('./uninstall.js')},
            {"from": Path.resolve('./module.json')},
            {"from": Path.resolve('./config/'), to: './config'},
          ]
        }
    ),
    new CloudCmsPlugin()
  ]
};
