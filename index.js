define(function(require) {
        // sample form fields    
    require("./fields/theme-id.js");
    require("./fields/blog-article-id.js");
    require("./fields/inline-components-array.js");
    require("./fields/inline-components-array-experimental.js");
    require("./fields/ih-text-resource-map-field.js");
    require("./fields/giata-list-uploader.js");
    require("./fields/feature-checkbox.js");
    require("./fields/page-id.js");
    require("./fields/radio-field-datasource-fix.js");
    require("./fields/campus-import-status.js");
    require("./fields/select-with-color.js");
    require("./fields/redirect-source.js");
    require("./fields/redirect-target.js");
    require("./gadgets/content-instances.js");
    require("./gadgets/hide-form-selection.js");
})