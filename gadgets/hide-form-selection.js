define(function (require, exports, module) {
  const $ = require("jquery");

  let Empty = require("ratchet/dynamic/empty");

  let UI = require("ui");

  $("body").on("cloudcms-ready", function () {
    if ($('#form-selector').val() !== 'default') {
      $('#form-selector').val('default').change();
    }

    $("#form-selector").parent().parent().hide();
  });
});