define(function (require, exports, module) {
  var Ratchet = require("ratchet/web");

  var ContentInstancesGadget = require("app/gadgets/project/content/content-instances");

  return Ratchet.GadgetRegistry.register("hotelplan-content-instances", ContentInstancesGadget.extend({

  }));
});