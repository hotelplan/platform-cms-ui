define(function (require, exports, module) {

    const $ = require("jquery");
    const Alpaca = $.alpaca;

    const blogArticleIdRegexp = /^b-[0-9]+$/;
    const pageSize = 100;

    var alpaca = require("alpaca");

    Alpaca.Fields.CustomBlogArticleIdField = Alpaca.Fields.SelectField.extend({

        getFieldType: function () {
            return "blog-article-id";
        },

        setup: function () {
            // set up base field
            this.base();

            this.options.validator = function (callback) {
                var value = this.getValue();
                if (!blogArticleIdRegexp.test(value)) {
                    callback({
                        "status": false,
                        "message": "The Blog Article id must match the following pattern: b-[0-9]"
                    });
                } else {
                    callback({
                        "status": true
                    });
                }
            };
        },

        // Query all blogArticles sequentially limiting by the pageSize
        queryBlogArticles: async function (pageNumber, articles, self) {
            await alpaca.globalContext.branch.queryNodes(
                {
                    "_type": "hp:blog-article-page",
                    "_fields": {"id": 1, "title": 1, "cms-ui-locale": 1}
                },
                {
                    "skip": pageNumber * pageSize,
                    "limit": pageSize
                })
                .then(function () {
                    if (this.asArray().length == 0) {
                        return articles;
                    } else {
                        this.asArray().forEach(el => articles.push(el));
                        return self.queryBlogArticles(pageNumber + 1, articles, self);
                    }
                })
        },

        // Blog Article ID datasource calculation
        prepareControlModel: function (callback) {
            var self = this;
            this.base(function (model) {
                let articles = [];
                self.queryBlogArticles(0, articles, self).then(function () {

                    //get project locales
                    var localeIdMap = new Map();
                    for (l of alpaca.globalContext.project.locales) {
                        localeIdMap.set(l.code, new Set());
                    }

                    let maxId = 0;
                    let allArticlesMap = new Map();
                    let noLocaleArticles = new Set()
                    for (article of articles) {
                        let id = Number(article.id.substring(2));
                        allArticlesMap.set(id, article);
                        let locale = article["cms-ui-locale"];
                        if (locale !== undefined && localeIdMap.has(locale)) {
                            localeIdMap.get(locale).add(id);
                        } else {
                            noLocaleArticles.add(id);
                        }
                        if (id > maxId) {
                            maxId = id;
                        }
                    }

                    // the result IDs collection
                    let dataSource = [];

                    //add current ID if exists
                    let currentValue = self.getValue();
                    let currentIdNumber = 0;
                    if (currentValue !== undefined && currentValue !== '') {
                        currentIdNumber = Number(currentValue.substring(2));
                        let noLocaleMessage = noLocaleArticles.has(currentIdNumber) ? 'Current node has no locale, please fix!' : '';
                        dataSource.push({
                            "value": currentValue,
                            "text": currentValue + ' [Current ID] ' + noLocaleMessage
                        });
                    }

                    //add next avaliable ID
                    let nextId = 'b-' + (maxId + 1);
                    dataSource.push({
                        "value": nextId,
                        "text": nextId + ' [New ID]'
                    });

                    //add the left IDs without a couple in other language
                    for (let i = 1; i <= maxId; i++) {
                        let allLocalesPresent = true;
                        for (idSet of localeIdMap.values()) {
                            allLocalesPresent = allLocalesPresent && idSet.has(i);
                        }
                        if (allLocalesPresent || currentIdNumber == i) {
                            continue;
                        }
                        if (allArticlesMap.has(i)) {
                            let a = allArticlesMap.get(i);
                            let value = a.id;
                            let text = value + ' [Existing node title: ' + a.title + ' (Locale: ' + a["cms-ui-locale"] + ')]';
                            if (noLocaleArticles.has(i)) {
                                text += ' A node without Locale found, please fix!';
                            }
                            dataSource.push({
                                "value": value,
                                "text": text
                            });
                        } else {
                            dataSource.push({
                                "value": 'b-' + i,
                                "text": 'b-' + i + ' [New ID]'
                            });
                        }
                    }
                    self.options.hideNone = true;
                    self.options.emptySelectFirst = true;
                    self.options.sort = false;
                    self.options.dataSource = dataSource;
                    callback(model);
                });
            })
        }
    });

    Alpaca.registerFieldClass("blog-article-id", Alpaca.Fields.CustomBlogArticleIdField);

});