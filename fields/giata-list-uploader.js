define(function (require, exports, module) {

    const $ = require("jquery");
    const Alpaca = $.alpaca;

    const giataListRegexp = /^(\s*[0-9]+\s*[,\n])*(\s*[0-9]+\s*)$/;
    const pageSize = 100;
    const productQnames = ["hp:hotel", "hp:cruise", "hp:roundtrip", "hp:adventuretravel"];

    Alpaca.Fields.CustomProductListUploaderByGiata = Alpaca.Fields.TextAreaField.extend({

        getFieldType: function () {
            return "product-list-uploader-by-giata";
        },

        postRender: function (callback) {
            var self = this;
            this.base(function () {
                $(self.domEl).find('textarea').after($('<a/>', {
                    "id": 'upload-giata-list',
                    "text": "Upload giata list",
                    "name": 'upload-giata-list-button',
                    "class": 'btn btn-default picker-button'
                }))
                callback();
            })
        },

        setup: function () {
            // set up base field
            this.base();

            this.options.validator = function (callback) {
                var self = this;
                var value = self.getValue();
                $('#upload-giata-list').off().removeClass("disabled");
                $('#upload-giata-list-message').remove();

                if (giataListRegexp.test(value)) {
                    // if not empty and correct input - upload is allowed

                    let giataList = value.replaceAll(/\n/g, ',')
                        .replaceAll(/\s/g, '')
                        .split(",")
                        .map(v => Number(v))
                        .filter(giata => giata !== 0);

                    $('#upload-giata-list').on("click", () => self.doUploadGiataList(self, giataList));

                    callback({
                        "status": true
                    });

                } else {
                    if (!value || value === "") {
                        // if empty input
                        callback({
                            "status": true
                        });
                    } else {
                        // if incorrect input
                        $('#upload-giata-list').addClass("disabled");
                        callback({
                            "status": false,
                            "message": "Giata list is expected: 111, 222, 333"
                        });
                    }
                }
            };
        },

        doUploadGiataList: function (self, giataList) {
            $('#upload-giata-list').off().addClass("disabled");
            self.control[0].disabled = true;
            $('#upload-giata-list-message').remove();
            $('#upload-giata-list').after($('<div/>', {
                "id": 'upload-giata-list-message',
                "text": "Loading...",
            }));

            let localeElement = self.parent.children.find(el => el.name === "cms-ui-locale");
            let cmsUiLocale = localeElement.data.length === 1 ? localeElement.data[0].value : "";
            let refBase = 'node://' + Alpaca.globalContext.platform.getId() + '/'
                + Alpaca.globalContext.repository.getId() + '/'
                + Alpaca.globalContext.branch.getId() + '/';

            let products = [];

            self.queryProducts(0, products, self, giataList, cmsUiLocale).then(function () {
                $('#upload-giata-list').removeClass("disabled");
                $('#upload-giata-list-message').remove();
                let relatorItems = products.map(product => {
                    var relator = {};
                    relator.id = product._doc;
                    relator.qname = "o:" + product._doc;
                    relator.ref = refBase + product._doc;
                    relator.title = product.title;
                    relator.typeQName = product._type;
                    return {
                        'relator': relator,
                        'code_giata': product.code_giata
                    };
                });

                if (relatorItems.length === 0) {
                    self.refresh();
                    if (giataList.length > 0) {
                        $('#upload-giata-list').off()
                            .on("click", () => self.doUploadGiataList(self, giataList));
                        $('#upload-giata-list').after($('<div/>', {
                            "id": 'upload-giata-list-message',
                            "text": "No products found by giata codes, please check giata list and locale.",
                        }));
                    }
                    return;
                }

                // add new relators list by giata to search parameters - product list
                let productsElement = self.parent.children
                    .find(el => el.name === 'search-parameters').children
                    .find(el => el.name === 'search-parameters_products');
                let currentProductIds = new Set();
                if (productsElement.data) {
                    productsElement.data.forEach(relator => currentProductIds.add(relator.id))
                } else {
                    productsElement.data = [];
                }
                let importedGiatas = new Set();
                for (let item of relatorItems) {
                    if (!currentProductIds.has(item.relator.id)) {
                        productsElement.data.push(item.relator);
                        importedGiatas.add(item.code_giata);
                    }
                }
                productsElement.refresh();

                // cleanup text area
                let ignoredGiatas = giataList.filter(giata => !importedGiatas.has(giata));
                let ignoredGiataString = giataList.filter(giata => !importedGiatas.has(giata)).join(', ');
                self.control[0].value = ignoredGiataString;
                self.data = ignoredGiataString;
                self.refresh();

                if (ignoredGiatas.length === giataList.length) {
                    $('#upload-giata-list').after($('<div/>', {
                        "id": 'upload-giata-list-message',
                        "text": "All found products were ignored, please check current product list",
                    }));
                    $('#upload-giata-list').off().on("click", () => self.doUploadGiataList(self, ignoredGiatas));
                    return;
                }

                if (ignoredGiatas.length > 0) {
                    $('#upload-giata-list').off().on("click", () => self.doUploadGiataList(self, ignoredGiatas));
                    $('#upload-giata-list').after($('<div/>', {
                        "id": 'upload-giata-list-message',
                        "text": "Successfully imported " + importedGiatas.size + " products. Some giata codes were ignored",
                    }));
                    return;
                }
                $('#upload-giata-list').off();
                $('#upload-giata-list').after($('<div/>', {
                    "id": 'upload-giata-list-message',
                    "text": "Successfully imported " + importedGiatas.size + " products.",
                }));
            })
        },

        // Query all listed products by giata sequentially limiting by the pageSize
        queryProducts: async function (pageNumber, products, self, giataList, cmsUiLocale) {
            if (!Array.isArray(giataList)) {
                return products;
            }
            await Alpaca.globalContext.branch.queryNodes(
                {
                    "_type": {"$in": productQnames},
                    "code_giata": {"$in": giataList},
                    "cms-ui-locale": cmsUiLocale,
                    "_fields": {"title": 1, "_doc": 1, "code_giata": 1, "_type": 1}
                },
                {
                    "skip": pageNumber * pageSize,
                    "limit": pageSize
                })
                .then(function () {
                    if (this.asArray().length == 0) {
                        return products;
                    } else {
                        this.asArray().forEach(el => products.push(el));
                        return self.queryProducts(pageNumber + 1, products, self, giataList, cmsUiLocale);
                    }
                })
        }
    });

    Alpaca.registerFieldClass("product-list-uploader-by-giata", Alpaca.Fields.CustomProductListUploaderByGiata);

});