define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  Alpaca.Fields.FeatureCheckBoxField = Alpaca.Fields.CheckBoxField.extend({

    getFieldType: function () {
      return "feature-checkbox";
    },

    setup: function () {
      // set up base field
      this.base();

      let self = this;
      if (!Alpaca.globalContext.document
          || !self.options.featureType) {
        self.options.disabled = true;
        self.options.hidden = true;
      }

      if (self.view && self.view.type === "edit") {
        self.options.rightLabel = self.options.label;
        self.options.label = undefined;
      }

      let val = Alpaca.globalContext.document
          && Alpaca.globalContext.document.hasFeature(
              self.options.featureType);

      self.setValue(val, true);
    },

    afterRenderControl: function (model, callback) {
      let self = this;
      this.base(model, function () {
        if (self.view && self.view.type === "edit") {
          self.field.css("margin", "0px");
          self.control.css("margin", "0px");
          let label = $("label", self.control);
          label.css("margin", "0px");
        }
        callback();
      });
    },

    onChange: function () {
      this.base();

      let self = this;
      let vals = self.data.map(d => d.value);
      let check = vals.includes(true);

      self.disable();
      if (check) {
        Alpaca.globalContext.document.addFeature(self.options.featureType)
        .then(function () {
          self.enable();
        });
      } else {
        Alpaca.globalContext.document.removeFeature(self.options.featureType)
        .then(function () {
          self.enable();
        });
      }

    }
  });

  Alpaca.registerFieldClass("feature-checkbox",
      Alpaca.Fields.FeatureCheckBoxField);
});