define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  const PAGE_SIZE = 100;
  const LOCALE_FIELD = "cms-ui-locale";

  const alpaca = require("alpaca");

  Alpaca.Fields.CustomPageIdField = Alpaca.Fields.SelectField.extend({

    getFieldType: function () {
      return "page-id";
    },

    setup: function () {
      let self = this;

      // set up base field
      self.base();

      if (!self.options.idPrefix) {
        throw new Error('idPrefix is not defined!');
      }

      let parent = self.parent;
      while (!!parent.parent) {
        parent = parent.parent;
      }

      if (parent.schema) {
        self.options.pageType = parent.schema._qname;
      }

      if (!self.options.pageType) {
        console.warn('page-id field: pageType not found!')
        return;
      }

      let validationPattern = "^" + self.options.idPrefix + "-[0-9]+$";

      self.options.validator = function (callback) {
        let value = self.getValue();
        let re = new RegExp(validationPattern);
        if (!re.test(value)) {
          callback({
            "status": false,
            "message": "The id must match the following pattern: "
                + validationPattern
          });
        } else {
          callback({
            "status": true
          });
        }
      };
    },

    // Query all pages sequentially limiting by the pageSize
    queryAllNodes: async function (pageNumber, nodes, self) {
      let query = {
        "_type": self.options.pageType,
        "_fields": {"title": 1, "cms-ui-locale": 1}
      };

      query._fields[self.name] = 1;

      await alpaca.globalContext.branch.queryNodes(
          query,
          {
            "skip": pageNumber * PAGE_SIZE,
            "limit": PAGE_SIZE
          })
      .then(function () {
        if (this.asArray().length == 0) {
          return nodes;
        } else {
          this.asArray().forEach(el => nodes.push(el));
          return self.queryAllNodes(pageNumber + 1, nodes, self);
        }
      })
    },

    // Page ID datasource calculation
    prepareControlModel: function (callback) {
      let self = this;
      self.base(function (model) {
        if (model.view && model.view.type === "display") {
          model.selectOptions.push({
            "value": self.getValue(),
            "text": self.getValue()
          });
          callback(model);
          return;
        }

        if(!self.options.pageType){
          callback(model);
          return;
        }

        let nodes = [];
        self.queryAllNodes(0, nodes, self).then(function () {

          //get project locales
          let localeIdMap = new Map();
          for (l of alpaca.globalContext.project.locales) {
            localeIdMap.set(l.code, new Set());
          }

          let maxId = 0;
          let allNodesMap = new Map();
          let noLocaleNodes = new Set()
          for (n of nodes) {
            if (!n[self.name]) {
              continue;
            }

            let id = Number(n[self.name].substring(2));

            if (!allNodesMap.has(id)) {
              allNodesMap.set(id, new Set());
            }
            let idSet = allNodesMap.get(id);
            idSet.add(n);
            let locale = n[LOCALE_FIELD];
            if (locale !== undefined && localeIdMap.has(locale)) {
              localeIdMap.get(locale).add(id);
            } else {
              noLocaleNodes.add(id);
            }
            if (id > maxId) {
              maxId = id;
            }
          }

          // the result IDs collection
          let dataSource = [];

          //add current ID if exists
          let currentValue = self.getValue();
          let currentIdNumber = 0;
          if (currentValue !== undefined && currentValue !== '') {
            currentIdNumber = Number(currentValue.substring(2));
            let noLocaleMessage = noLocaleNodes.has(currentIdNumber)
                ? 'Current node has no locale, please fix!' : '';

            let linkedNodeMessage = '';
            if (allNodesMap.has(currentIdNumber)) {
              for (n of allNodesMap.get(currentIdNumber).values()) {
                if (alpaca.globalContext.document
                    && alpaca.globalContext.document['_doc'] === n['_doc']) {
                  continue;
                }
                linkedNodeMessage += ' [translation of: ' + n.title;
                linkedNodeMessage += !!n[LOCALE_FIELD]
                    ? ' (' + n[LOCALE_FIELD] + ')'
                    : '(Locale not set)';

                linkedNodeMessage += ']';
              }
            }

            dataSource.push({
              "value": currentValue,
              "text": currentValue + ' [Current ID] ' + noLocaleMessage
                  + linkedNodeMessage
            });
          }

          //add next available ID
          let nextId = self.options.idPrefix + '-' + (maxId + 1);
          dataSource.push({
            "value": nextId,
            "text": nextId + ' [New ID]'
          });

          let firstNewIdFound = false;

          //add the left IDs without a couple in other language
          for (let i = maxId; i > 0; i--) {
            let allLocalesPresent = true;
            for (idSet of localeIdMap.values()) {
              allLocalesPresent = allLocalesPresent && idSet.has(i);
            }
            if (allLocalesPresent || currentIdNumber == i) {
              continue;
            }
            if (allNodesMap.has(i)) {
              let value = self.options.idPrefix + '-' + i;
              let text = value;
              for (n of allNodesMap.get(i).values()) {
                text += ' [translation of: ' + n.title;

                text += !!n[LOCALE_FIELD]
                    ? ' (' + n[LOCALE_FIELD] + ')'
                    : ' (Locale not set, please fix!)';

                text += ']';
              }
              dataSource.push({
                "value": value,
                "text": text
              });

            } else {
              if (!firstNewIdFound) {
                dataSource.push({
                  "value": self.options.idPrefix + '-' + i,
                  "text": self.options.idPrefix + '-' + i + ' [New ID]'
                });
              }
              firstNewIdFound = true;
            }
          }
          self.options.hideNone = true;
          self.options.emptySelectFirst = true;
          self.options.sort = false;
          self.options.dataSource = dataSource;
          callback(model);
        });
      })
    }
  });

  Alpaca.registerFieldClass("page-id", Alpaca.Fields.CustomPageIdField);

});