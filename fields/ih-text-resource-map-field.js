define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  Alpaca.Fields.IhTextResourceMapField = Alpaca.Fields.ArrayField.extend({

    getFieldType: function () {
      return "ih-text-resource-map-field";
    },

    setup: function () {
      // set up base field
      this.base();

      let self = this;
      if (self.data && self.data.length) {
        return;
      }

      let locales = Alpaca.globalContext.project.locales;

      for (let i = 0; i < locales.length; i++) {
        self.data.push({'language': locales[i].code});
      }
    },
  });

  Alpaca.registerFieldClass("ih-text-resource-map-field",
      Alpaca.Fields.IhTextResourceMapField);

});