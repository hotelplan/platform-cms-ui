define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  Alpaca.Fields.SelectWithColorsField = Alpaca.Fields.SelectField.extend({

    getFieldType: function () {
      return "select-with-colors";
    },

    afterRenderControl: function (model, callback) {
      let self = this;
      this.base(model, function () {
        for (let i = 0; i < self.selectOptions.length; i++) {
          let option = self.selectOptions[i];
          let optionValues = option.value.split("_"); //bgColor_fontColor_themeName e.g #FFFFFF_#D6F0C9_LIGHT
          let bgColor = optionValues[0];
          let fontColor = optionValues[1];
          let htmlOption = $("option", self.domEl)[i];
          $(htmlOption).css({"background-color": bgColor, "color": fontColor});
          //set text and color to div
          if (option.selected) {
            $("div.alpaca-control", self.domEl).css({"background-color": bgColor, "color": fontColor});
            $("div.alpaca-control", self.domEl).html(option.text);
          }
        }
        callback();
      });
    },

  });

  Alpaca.registerFieldClass("select-with-colors",
      Alpaca.Fields.SelectWithColorsField);
});