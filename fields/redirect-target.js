define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  Alpaca.Fields.CustomRedirectTargetField = Alpaca.Fields.TextField.extend({

    getFieldType: function () {
      return "redirect-target";
    },

    setup: function () {
      // set up base field
      this.base();

      this.options.validator = function (callback) {
        let value = this.getValue();
        let valid = true;
        let message = '';

        if (value.includes(' ')) {
          valid = false;
          message = 'cannot contain whitespaces'
        }

        if (!valid) {
          callback({
            "status": false,
            "message": message
          });
        } else {
          callback({
            "status": true
          });
        }
      };

    }
  });

  Alpaca.registerFieldClass("redirect-target",
      Alpaca.Fields.CustomRedirectTargetField);

});