define(function (require, exports, module) {

    const $ = require("jquery");
    const Alpaca = $.alpaca;

    Alpaca.Fields.InlineComponentsArray = Alpaca.Fields.ArrayField.extend({

        getFieldType: function () {
            return "inline-components-array";
        },

        setup: function () {
            // set up base field
            this.base();
        },

        // !!! callback is set to makeCollapsible
        addItem: function (index, schema, options, data, callback) {
            var self = this;
            if (self._validateEqualMaxItems()) {
                self.triggerWithPropagation("before_nested_change");

                // fix the radio fields reset issue
                //
                // before new DOM control is added to the "index" position
                // the already existing control on this position "index" must change the name
                //
                // other vice when browser adds DOM controls for radio buttons at position "index"
                // the name of old and new controls will be the same, so browser will
                // lose the old values of existing controls

                // loop starts from the end of the array
                for (let nextIndex = self.children.length - 1; nextIndex >= index; nextIndex--) {
                    let child = self.children[nextIndex];
                    // new position
                    let i = nextIndex + 1;

                    if (self.schema.type === "array") {
                        child.path = self.path + "[" + i + "]"
                    } else if (!!child.propertyId) {
                        child.path = self.path + "/" + child.propertyId
                    }
                    child.name = undefined;
                    child.calculateName();
                    $(child.containerItemEl).attr("data-alpaca-container-item-index", i);
                    $(child.containerItemEl).attr("data-alpaca-container-item-name", child.name);
                    child.updateDOMElement()
                }

                self.createItem(index, schema, options, data, function (item) {
                    self.registerChild(item, index);
                    self.doAddItem(index, item, function () {
                        self.handleRepositionDOMRefresh();
                        self.updateToolbars();
                        self.refreshValidationState();
                        self.trigger("add", item);
                        self.triggerUpdate();
                        self.onChange.call(self);
                        self.triggerWithPropagation("change");
                        self.triggerWithPropagation("after_nested_change");
                        if (callback) {
                            Alpaca.nextTick(function () {
                                self.makeCollapsible(self);
                                callback(item)
                            })
                        }
                    })
                })
            }
        },

        // Method is a modified copy from cms ArrayField.js
        handleActionBarAddItemClick: function (itemIndex, callback) {
            var self = this;

            self.resolveItemSchemaOptions(function (itemSchema, itemOptions, circularityCheck) {

                // we only allow addition if the resolved schema isn't circularly referenced
                // or the schema is optional
                if (circularityCheck && circularityCheck.circular) {
                    circularityCheck.object = self.top().options;
                    return Alpaca.throwReferenceCircularityError(circularityCheck, self.errorCallback);
                }

                var itemData = Alpaca.createEmptyDataInstance(itemSchema);
                self.addItem(itemIndex + 1, itemSchema, itemOptions, itemData, function (item) {
                    if (callback) {
                        callback(item);
                    }
                });
            });
        },

        // Items colorization methods
        doAfterAddItem: function (item, callback) {
            var self = this;
            this.base(item, function () {
                self.colorizeArrayElements(self);
                callback();
            });
        },

        doAfterRemoveItem: function (childIndex, callback) {
            var self = this;
            this.base(childIndex, function () {
                self.colorizeArrayElements(self);
                callback();
            });
        },

        postRender: function (callback) {
            var self = this;
            this.base(function () {
                self.colorizeArrayElements(self);
                self.makeCollapsible(self);
                callback()
            })
        },

        colorizeArrayElements: function (self) {
            if (!self.children || self.children.length === 0) {
                return;
            }
            for (let i = 0; i < self.children.length; i++) {
                if (i % 2 == 0) {
                    $(self.children[i].containerItemEl).css('background-color', 'rgb(230,230,220)');
                } else {
                    $(self.children[i].containerItemEl).css('background-color', 'rgb(230,230,240)');
                }
            }
        },

        // Collapse inline items
        makeCollapsible: function (self) {
            if (!self.children || self.children.length === 0) {
                return;
            }
            for (let i = 0; i < self.children.length; i++) {
                self.makeArrayItemItemCollapsible(self.children[i], self);
            }
        },

        makeArrayItemItemCollapsible: function (model, self) {
            if (!model) {
                return;
            }
            let fieldContainer = model.containerItemEl.children().last().children().last();
            if (!fieldContainer.hasClass('alpaca-field')) {
                return;
            }

            let actionBar = $('.alpaca-array-actionbar',
                model.containerItemEl).first();
            let buttonControl = $('.collapse-inline-element-button',
                model.containerItemEl);
            if (!actionBar.length || buttonControl.length) {
                return;
            }

            let data = model.data;
            let collapseInlineElement = $('<a>', {
                class: "alpaca-array-actionbar-action btn btn-default btn-sm collapse-inline-element-button"
            })
            .html(self.getItemSummary(model))
            .click(function () {
                let collapsedCheckboxOnClick = model.children.find(
                    v => v.propertyId === "collapsed");
                if (collapsedCheckboxOnClick) {
                    if (collapsedCheckboxOnClick.getValue()) {
                        self.showField(fieldContainer);
                    } else {
                        self.collapseField(fieldContainer);
                    }
                    collapsedCheckboxOnClick.setValue(
                        !collapsedCheckboxOnClick.getValue());
                }
                let buttonLabel = $('.collapse-inline-element-button',
                    model.containerItemEl)
                buttonLabel.html(self.getItemSummary(model));
            });
            actionBar.append(collapseInlineElement);

            let isCollapsed;
            let collapsedCheckbox = model.children.find(
                v => v.propertyId === "collapsed");
            if (collapsedCheckbox) {
                isCollapsed = collapsedCheckbox.getValue();
            } else {
                isCollapsed = data && data.collapsed;
            }

            if (isCollapsed) {
                self.collapseField(fieldContainer);
            } else {
                self.showField(fieldContainer);
            }
        },

        collapseField: function (field) {
            if (!field) {
                return;
            }
            field.css({ 'visibility' : 'hidden', 'height' : '0px' });
            field.attr('style', function(i,s) { return (s || '') + 'padding: 0px !important;' });
        },

        showField: function (field) {
            if (!field) {
                return;
            }
            field.css({ 'visibility' : '', 'height' : '', 'padding' : '' });
        },

        getItemSummary: function (model) {
            let summary = 'RELATOR - EMPTY';
            if (!model) {
                return summary;
            }

            let componentKindControl = model.children.find(
                v => v.propertyId === "component-kind");
            if (!componentKindControl) {
                return summary;
            }
            let componentKind = componentKindControl.getValue();

            if (componentKind === "COMPONENT_RELATOR") {
                let relatorControl = model.children.find(
                    v => v.propertyId === "component-relator");
                if (!relatorControl) {
                    return summary;
                }

                let relator = relatorControl.getValue();
                if (relator) {
                    summary = "RELATOR - ";
                    if (relator.typeQName) {
                        summary = summary + relator.typeQName.substring(3)
                            + " - ";
                    }
                    summary = summary + relator.title;
                } else {
                    summary = 'RELATOR - EMPTY';
                }
            } else if (componentKind === "COMPONENT_INLINE") {
                let inlineControl = model.children.find(
                    v => v.propertyId === "component-inline");
                if (!inlineControl) {
                    return summary;
                }

                let inline = inlineControl.getValue();
                if (inline) {
                    let title = '';
                    for (key in inline) {
                        if (inline[key].hasOwnProperty('title')) {
                            title = inline[key].title;
                        }
                    }
                    summary = "INLINE - " + inline['component-type'] + " - "
                        + title;
                } else {
                    summary = 'INLINE - EMPTY';
                }
            }
            return summary;
        },

        // Confirmation dialog when item is removed, !!! callback is set to makeCollapsible
        removeItem: function (childIndex, callback, force, confirmed) {
            // when answer was Yes or Bootstrap unavailable - call standard method
            var self = this;
            if (confirmed || (typeof $.fn.modal !== 'function')) {
                this.base(childIndex, function () {
                    self.makeCollapsible(self);
                    callback();
                }, force);
                return;
            }

            $(`<div class="modal fade" id="confirmationModal" role="dialog"> 
             <div class="modal-dialog"> 
                <div class="modal-content"> 
                   <div class="modal-body" style="padding:10px;"> 
                     <h4 class="text-center">Component item will be removed. Are you sure?</h4> 
                     <div class="text-center"> 
                       <a class="btn btn-primary btn-yes">Yes</a> 
                       <a class="btn btn-default btn-no">No</a> 
                     </div> 
                   </div> 
               </div> 
            </div> 
          </div>`).appendTo(self.domEl);

            //Remove the modal once it is closed.
            $("#confirmationModal").on('hidden.bs.modal', function () {
                $("#confirmationModal").remove();
            });

            //Confirmation successful
            $(".btn-yes").on("click", function () {
                self.removeItem(childIndex, callback, force, true);
                $("#confirmationModal").modal("hide");
            });

            //Cancelled
            $(".btn-no").click(function () {
                $("#confirmationModal").modal("hide");
            });

            //Trigger the modal
            $("#confirmationModal").modal({
                backdrop: 'static',
                keyboard: false
            });
        }

    });

    Alpaca.registerFieldClass("inline-components-array",
        Alpaca.Fields.InlineComponentsArray);

});