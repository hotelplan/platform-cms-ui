define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  Alpaca.Fields.CustomRedirectSourceField = Alpaca.Fields.TextField.extend({

    getFieldType: function () {
      return "redirect-source";
    },

    setup: function () {
      // set up base field
      this.base();

      this.options.validator = function (callback) {
        let value = this.getValue();
        let valid = true;
        let message = '';

        if (!value.startsWith('/')) {
          valid = false;
          message = 'must start with a "/" (no http/https or any domain names like www.domain.com)'
        }

        if (value.includes(' ')) {
          valid = false;
          message = 'cannot contain whitespaces'
        }

        if (value.includes('?')) {
          valid = false;
          message = 'cannot contain query string. Please remove everything after "?" (including "?")'
        }

        if (!valid) {
          callback({
            "status": false,
            "message": message
          });
        } else {
          callback({
            "status": true
          });
        }
      };

    }
  });

  Alpaca.registerFieldClass("redirect-source",
      Alpaca.Fields.CustomRedirectSourceField);

});