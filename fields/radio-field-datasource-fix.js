define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  Alpaca.Fields.RadioFieldDatasourceFix = Alpaca.Fields.RadioField.extend({

    getFieldType: function () {
      return "radio-field-datasource-fix";
    },

    beforeRenderControl: function (model, callback) {
      let self = this;

      let completionFn = function () {
        let optionWasSelectedByData = false;
        for (let i = 0; i < self.selectOptions.length; i++) {
          for (let j = 0; j < self.data.length; j++) {
            if (self.data[j].value === self.selectOptions[i].value) {

              self.selectOptions[i].selected = true;
              optionWasSelectedByData = true;
            } else {
              self.selectOptions[i].selected = false;
            }
          }
        }

        if (!optionWasSelectedByData) {
          for (let i = 0; i < self.selectOptions.length; i++) {
            if (self.schema.default === self.selectOptions[i].value) {
              self.selectOptions[i].selected = true;
            }
          }
        }

        // if emptySelectFirst and we have options but no data, then auto-select first item in the options list
        if (self.data.length === 0 && self.options.emptySelectFirst
            && self.selectOptions.length > 0) {
          self.selectOptions[0].selected = true;
          self.data = [self.selectOptions[0]];

          self.updateObservable();
          self.triggerUpdate();
          self.triggerWithPropagation("change");
        }

        // likewise, we auto-assign first pick if field required
        if (self.data.length === 0 && self.isRequired()
            && self.selectOptions.length > 0) {
          self.selectOptions[0].selected = true;
          self.data = [self.selectOptions[0]];

          self.updateObservable();
          self.triggerUpdate();
          self.triggerWithPropagation("change");
        }

        callback();
      };

      this.base(model, function () {

        self.populateDisplayableText(model);

        if (self.options.dataSource) {
          // clear the array
          self.selectOptions.length = 0;

          self.invokeDataSource(self.selectOptions, model, function () {

            if (self.options.useDataSourceAsEnum) {
              // now build out the enum and optionLabels
              let _enum = [];
              let _optionLabels = [];
              for (let i = 0; i < self.selectOptions.length; i++) {
                _enum.push(self.selectOptions[i].value);
                _optionLabels.push(self.selectOptions[i].text);
              }

              self.setEnum(_enum);
              self.setOptionLabels(_optionLabels);
            }

            completionFn();

          });
        } else {
          completionFn();
        }

      });
    },
  });

  Alpaca.registerFieldClass("radio-field-datasource-fix",
      Alpaca.Fields.RadioFieldDatasourceFix);

});