define(function (require, exports, module) {

  const $ = require("jquery");
  const Alpaca = $.alpaca;

  const PAGE_SIZE = 1;

  const GROUP_CLASS = "campus-import-status-group";
  const SPINNER_ID = "campus-import-status-spinner";
  const TEXTAREA_ID = "campus-import-status-textarea";
  const REFRESH_BUTTON_ID = "campus-import-status-refresh";
  const INITIAL_PAGE_LOAD = "campus-import-status-initial-page-load";

  Alpaca.Fields.CampusImportStatusField = Alpaca.Fields.TextAreaField.extend({

    getFieldType: function () {
      return "campus-import-status";
    },

    setup: function () {
      // set up base field
      this.base();

      let self = this;

      let campusId;
      let lang;

      if (Alpaca.globalContext.document) {
        campusId = Alpaca.globalContext.document.campus_id;
        let releaseTime = Alpaca.globalContext.document.release_time;
        let localeFeature = Alpaca.globalContext.document.getFeature("f:locale");
        lang = null;
        if (localeFeature) {
          lang = Alpaca.globalContext.document.getFeature("f:locale").locale;
        }

        self.options.campusId = campusId;
        self.options.releaseTime = releaseTime;
        self.options.lang = lang;
      }

      if (!campusId || !lang) {
        self.options.disabled = true;
        self.options.hidden = true;
      }
    },

    postRender: function (callback) {
      let self = this;
      this.base(function () {
        let label = null;
        let last = null;

        let field = $('.form-group', self.domEl);
        field.addClass(GROUP_CLASS);

        field.children().each(function () {
          let item = $(this);
          if (item.prop("tagName") !== "LABEL") {
            item.hide();
          } else {
            label = item;
          }
          last = item;
        });

        if (!label
            || !self.options.campusId
            || !self.options.lang) {
          field.children().hide();
          self.options.disabled = true;
          self.options.hidden = true;
          callback();
          return;
        }

        let cellClass = "col-sm-9";
        if (self.view && self.view.type === "edit") {
          cellClass = "";
        }

        let cell = $('<div></div>', {
          "class": cellClass
        });

        cell.append($('<div></div>', {
          "id": SPINNER_ID,
          "name": 'campus-import-status-spinner',
          "class": 'sk-spinner sk-spinner-pulse'
        }))
        .append($('<textarea></textarea>', {
          "id": TEXTAREA_ID,
          "class": 'alpaca-control form-control',
          "style": 'display: none; border: 5px solid #ff0000 !important;',
          "rows": "5"
        }))
        .append($('<a/>', {
          "id": REFRESH_BUTTON_ID,
          "text": "Refresh",
          "name": 'campus-import-status-refresh-button',
          "class": 'btn btn-default picker-button',
          "style": 'display: none;'
        }))
        .append($('<input/>', {
          "id": INITIAL_PAGE_LOAD,
          "type": "hidden",
          "value": true
        }));

        last.after(cell);

        $('#' + REFRESH_BUTTON_ID, field).on("click",
            () => self.checkStatus(self));

        self.checkStatus(self);

        callback();
      })
    },

    checkStatus: function (sender) {
      let self = this;

      $('#' + SPINNER_ID).show();
      $('#' + TEXTAREA_ID).hide();
      $('#' + REFRESH_BUTTON_ID).hide();

      let nodes = [];
      self.queryOneLog(
          self.options.campusId,
          self.options.lang,
          self.options.releaseTime,
          0, nodes, self).then(function () {

        let initialPageLoadInput = $('#' + INITIAL_PAGE_LOAD);

        if (nodes.length === 0) {
          let isInitialPageLoad = initialPageLoadInput.val();
          if (isInitialPageLoad === "true") {
            $('.' + GROUP_CLASS).hide();
            return;
          }

          nodes.push({
            "log": "Node has been updated. Please refresh the page."
          })
        }

        initialPageLoadInput.val(false);

        $('#' + SPINNER_ID).hide();
        $('#' + REFRESH_BUTTON_ID).show();

        let area = $('#' + TEXTAREA_ID);
        area.show();
        area.text(nodes[0].log);
        area.scrollTop(area.prop('scrollHeight'));

      });
    },

    // Query log
    queryOneLog: async function (campusId, lang, releaseTime, pageNumber, nodes,
        self) {
      if (!campusId || !lang && !releaseTime) {
        return;
      }

      let freshNode = null;

      await Alpaca.globalContext.branch.readNode(
          Alpaca.globalContext.document._doc)
      .then(function () {
        freshNode = this;

        let query = {
          "_type": "hp:import-log",
          "log_campus_id": campusId,
          "lang": lang,
          "release_time": {
            "$gte": freshNode.release_time
          },
          "_fields": {"title": 1, "_doc": 1, "log": 1}
        };

        let sort = {
          "_system.created_on.iso_8601": -1
        }

        return Alpaca.globalContext.branch.queryNodes(
            query,
            {
              "skip": pageNumber * PAGE_SIZE,
              "limit": PAGE_SIZE,
              "sort": encodeURI(JSON.stringify(sort))
            })
        .then(function () {
          if (this.asArray().length == 0) {
            return nodes;
          } else {
            this.asArray().forEach(el => nodes.push(el));
            return nodes;
          }
        })

      })
    },

  });

  Alpaca.registerFieldClass("campus-import-status",
      Alpaca.Fields.CampusImportStatusField);

});